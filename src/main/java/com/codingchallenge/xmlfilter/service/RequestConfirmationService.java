package com.codingchallenge.xmlfilter.service;

import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.codingchallenge.xmlfilter.exception.ApplicationException;
import com.codingchallenge.xmlfilter.filter.DefaultFilter;
import com.codingchallenge.xmlfilter.filter.FilterFactory;
import com.codingchallenge.xmlfilter.repository.RequestConfirmationRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RequestConfirmationService {

    public static final String BUYER_PARTY_REFERENCE_HREF = "//buyerPartyReference/@href";
    public static final String SELLER_PARTY_REFERENCE_HREF = "//sellerPartyReference/@href";
    public static final String PAYMENT_AMOUNT_AMOUNT = "//paymentAmount/amount";
    public static final String PAYMENT_AMOUNT_CURRENCY = "//paymentAmount/currency";

    @Autowired
    private RequestConfirmationRepository requestConfirmationRepository;

    @Autowired
    private FilterFactory filterFactory;

    public void save(String requestConfirmationString) {
        try {

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.parse(new ByteArrayInputStream(requestConfirmationString.getBytes("UTF-8")));

            String buyerParty = getValue(document, BUYER_PARTY_REFERENCE_HREF);
            String sellerParty = getValue(document, SELLER_PARTY_REFERENCE_HREF);
            String premiumAmount = getValue(document, PAYMENT_AMOUNT_AMOUNT);
            String premiumCurrency = getValue(document, PAYMENT_AMOUNT_CURRENCY);

            RequestConfirmation requestConfirmation = RequestConfirmation.builder().
                    buyerParty(buyerParty).
                    sellerParty(sellerParty).
                    premiumAmount(new BigDecimal(premiumAmount))
                    .premiumCurrency(premiumCurrency).build();

            requestConfirmationRepository.save(requestConfirmation);

        } catch(SAXException saxException) {
            throw new ApplicationException(saxException);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }

    }

    public List<RequestConfirmation> getRequestConfirmations(String filterId) {

        Predicate<RequestConfirmation> predicate = filterFactory.getFilter(filterId);

        return requestConfirmationRepository.findAll().stream().filter(predicate).collect(Collectors.toList());
    }

    private String getValue(Document document, String xpathExpression) throws XPathExpressionException {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile(xpathExpression);

        NodeList nodeList = (NodeList) expr.evaluate(document, XPathConstants.NODESET);

        return nodeList.item(0).getTextContent();
    }
}
