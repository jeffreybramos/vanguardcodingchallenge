package com.codingchallenge.xmlfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlfilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmlfilterApplication.class, args);
	}

}
