package com.codingchallenge.xmlfilter.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestConfirmationResponse {

        private String buyerParty;

        private String sellerParty;

        private BigDecimal premiumAmount;

        private String premiumCurrency;

}
