package com.codingchallenge.xmlfilter.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestConfirmationListResponse {

    private List<RequestConfirmationResponse> requestConfirmationResponses;
}
