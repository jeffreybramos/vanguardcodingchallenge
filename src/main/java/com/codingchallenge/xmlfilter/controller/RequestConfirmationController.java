package com.codingchallenge.xmlfilter.controller;

import com.codingchallenge.xmlfilter.controller.response.RequestConfirmationListResponse;
import com.codingchallenge.xmlfilter.controller.response.RequestConfirmationResponse;
import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.codingchallenge.xmlfilter.service.RequestConfirmationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/events")
@AllArgsConstructor
public class RequestConfirmationController {

    @Autowired
    private RequestConfirmationService requestConfirmationService;

    @GetMapping(value = "/filter/{filterId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequestConfirmationListResponse> getEvents(@PathVariable String filterId) {
        List<RequestConfirmation> requestConfirmations = requestConfirmationService.getRequestConfirmations(filterId);
        List<RequestConfirmationResponse> requestConfirmationResponses = requestConfirmations.stream().map(requestConfirmation ->
                RequestConfirmationResponse.builder().
                        buyerParty(requestConfirmation.getBuyerParty()).
                        sellerParty(requestConfirmation.getSellerParty()).
                        premiumAmount(requestConfirmation.getPremiumAmount()).
                        premiumCurrency(requestConfirmation.getPremiumCurrency())
                        .build()).collect(Collectors.toList());
        RequestConfirmationListResponse requestConfirmationListResponse = RequestConfirmationListResponse.builder().requestConfirmationResponses(requestConfirmationResponses).build();
        return ResponseEntity.ok().body(requestConfirmationListResponse);
    }

    @PostMapping(consumes={MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity saveEvent(@RequestBody String xml) {

        requestConfirmationService.save(xml);

        return ResponseEntity.ok().build();
    }

}
