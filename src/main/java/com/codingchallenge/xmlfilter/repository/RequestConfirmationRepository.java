package com.codingchallenge.xmlfilter.repository;

import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestConfirmationRepository extends JpaRepository<RequestConfirmation, Long> {
}
