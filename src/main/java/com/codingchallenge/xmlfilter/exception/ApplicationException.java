package com.codingchallenge.xmlfilter.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ApplicationException extends RuntimeException{
    private Exception exception;

    public ApplicationException(String errorMessage){
        super(errorMessage);
    }

}
