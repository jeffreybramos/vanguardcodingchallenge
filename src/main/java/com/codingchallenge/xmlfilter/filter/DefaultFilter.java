package com.codingchallenge.xmlfilter.filter;

import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import java.util.function.Predicate;

public class DefaultFilter implements Predicate<RequestConfirmation> {

    public static final String EMU_BANK = "EMU_BANK";
    public static final String AUD = "AUD";
    public static final String BISON_BANK = "BISON_BANK";
    public static final String USD = "USD";

    @Override
    public boolean test(RequestConfirmation requestConfirmation) {
        return !isAnagram(requestConfirmation.getSellerParty(), requestConfirmation.getBuyerParty()) && EMU_BANK.equals(requestConfirmation.getSellerParty()) && AUD.equals(requestConfirmation.getPremiumCurrency())
                || BISON_BANK.equals(requestConfirmation.getSellerParty()) && USD.equals(requestConfirmation.getPremiumCurrency());
    }

    boolean isAnagram(String value1, String value2) {
        if (value1.length() != value2.length()) {
            return false;
        }
        Multiset<Character> multiset1 = HashMultiset.create();
        Multiset<Character> multiset2 = HashMultiset.create();
        for (int i = 0; i < value1.length(); i++) {
            multiset1.add(value1.charAt(i));
            multiset2.add(value2.charAt(i));
        }
        return multiset1.equals(multiset2);
    }
}
