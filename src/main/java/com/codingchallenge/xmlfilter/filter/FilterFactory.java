package com.codingchallenge.xmlfilter.filter;

import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.codingchallenge.xmlfilter.exception.ApplicationException;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Predicate;

@Component
public class FilterFactory {

    public Predicate<RequestConfirmation> getFilter(String filterId) {

        if(filterId == null) {
            throw new ApplicationException("Filter Id is required");
        }

        if("1".equals(filterId)) {
            return new DefaultFilter();
        } else {
            throw new ApplicationException("Invalid filter id");
        }

    }
}
