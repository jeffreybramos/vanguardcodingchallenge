package com.codingchallenge.xmlfilter.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestConfirmation {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String buyerParty;

    @Column
    private String sellerParty;

    @Column
    private BigDecimal premiumAmount;

    @Column
    private String premiumCurrency;

}
