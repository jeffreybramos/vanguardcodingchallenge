package com.codingchallenge.xmlfilter.service;

import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.codingchallenge.xmlfilter.filter.DefaultFilter;
import com.codingchallenge.xmlfilter.filter.FilterFactory;
import com.codingchallenge.xmlfilter.repository.RequestConfirmationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@ExtendWith(MockitoExtension.class)
public class RequestConfirmationServiceTest {

    private RequestConfirmationService requestConfirmationService;

    @Mock
    private RequestConfirmationRepository requestConfirmationRepository;

    @Mock
    private FilterFactory filterFactory;

    public static final String TEST_FILTERID = "test-filterid";
    public static final String TEST_BUYER_1 = "test-buyer1";
    public static final String TEST_SELLER_1 = "test-seller1";
    public static final String TEST_BUYER_2 = "test-buyer2";
    public static final String TEST_SELLER_2 = "test-seller2";

    @Test
    public void testSave() {

        requestConfirmationService = new RequestConfirmationService(requestConfirmationRepository, filterFactory);

        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("classpath:data/event0.xml");

        requestConfirmationService.save(asString(resource));

        Mockito.verify(requestConfirmationRepository).save(Mockito.any(RequestConfirmation.class));
    }

    @Test
    public void testGet() {

        List<RequestConfirmation> requestConfirmations = new ArrayList<>();

        requestConfirmations.add(RequestConfirmation.builder().
                buyerParty(TEST_BUYER_1).
                sellerParty(TEST_SELLER_1).
                premiumAmount(BigDecimal.valueOf(100)).
                premiumCurrency("AUD").
                build());

        requestConfirmations.add(RequestConfirmation.builder().
                buyerParty(TEST_BUYER_2).
                sellerParty(TEST_SELLER_2).
                premiumAmount(BigDecimal.valueOf(200)).
                premiumCurrency("USD").
                build());

        Mockito.when(requestConfirmationRepository.findAll()).thenReturn(requestConfirmations);
        Mockito.when(filterFactory.getFilter("test-filterId")).thenReturn((requestConfirmation) -> { return true; });

        requestConfirmationService = new RequestConfirmationService(requestConfirmationRepository, filterFactory);

        List<RequestConfirmation> requestConfirmationsReturned = requestConfirmationService.getRequestConfirmations("test-filterId");

        Assertions.assertEquals(requestConfirmations, requestConfirmationsReturned);
    }

    public static String asString(Resource resource) {
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
