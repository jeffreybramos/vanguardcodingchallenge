package com.codingchallenge.xmlfilter.filter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultFilterTest {

    private DefaultFilter defaultFilter;

    @Test
    public void testIsAnagram() {

        defaultFilter = new DefaultFilter();

        Assertions.assertTrue(defaultFilter.isAnagram("ANAGRAM", "ANAGRAM"));
    }

    @Test
    public void testIsNotAnagram() {

        defaultFilter = new DefaultFilter();

        Assertions.assertFalse(defaultFilter.isAnagram("ANAGRAM", "NOT_ANAGRAM"));
    }

}
