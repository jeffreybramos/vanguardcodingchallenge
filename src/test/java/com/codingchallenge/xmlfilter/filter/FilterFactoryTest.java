package com.codingchallenge.xmlfilter.filter;

import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.codingchallenge.xmlfilter.exception.ApplicationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

public class FilterFactoryTest {

    private FilterFactory filterFactory;

    @Test
    public void testReturnsDefaultFilter() {

        filterFactory = new FilterFactory();

        Predicate<RequestConfirmation> filter = filterFactory.getFilter("1");

        Assertions.assertEquals(DefaultFilter.class, filter.getClass());
    }

    @Test
    public void testThrowsForInvalidFilterId() {

        filterFactory = new FilterFactory();

        ApplicationException thrown = Assertions.assertThrows(ApplicationException.class, () -> {
            filterFactory.getFilter("6");
        });

        Assertions.assertEquals("Invalid filter id", thrown.getMessage());
    }

    @Test
    public void testThrowsForFilterIdRequired() {

        filterFactory = new FilterFactory();

        ApplicationException thrown = Assertions.assertThrows(ApplicationException.class, () -> {
            filterFactory.getFilter(null);
        });

        Assertions.assertEquals("Filter Id is required", thrown.getMessage());
    }
}
