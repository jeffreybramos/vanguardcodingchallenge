package com.codingchallenge.xmlfilter.controller;

import com.codingchallenge.xmlfilter.controller.response.RequestConfirmationListResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RequestConfirmationControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Value("classpath:data/event0.xml")
    private Resource event0;

    @Value("classpath:data/event1.xml")
    private Resource event1;

    @Value("classpath:data/event2.xml")
    private Resource event2;

    @Value("classpath:data/event3.xml")
    private Resource event3;

    @Value("classpath:data/event4.xml")
    private Resource event4;

    @Value("classpath:data/event5.xml")
    private Resource event5;

    @Value("classpath:data/event6.xml")
    private Resource event6;

    @Value("classpath:data/event7.xml")
    private Resource event7;

    @BeforeEach
    public void before() {
        RestAssured.port = port;
    }

    @Test
    public void test() throws JsonProcessingException {
        String requestBody = asString(event0);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event1);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event2);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event3);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event4);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event5);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event6);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        requestBody = asString(event7);
        given().contentType(ContentType.XML).body(requestBody).post("/events").then().statusCode(200);

        RequestConfirmationListResponse response = new ObjectMapper().readValue(get("/events/filter/1").body().prettyPrint(), RequestConfirmationListResponse.class);

        Assertions.assertEquals(4, response.getRequestConfirmationResponses().size());
    }


    public static String asString(Resource resource) {
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
