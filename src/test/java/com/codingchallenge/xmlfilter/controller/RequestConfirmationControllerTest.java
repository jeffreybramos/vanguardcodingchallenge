package com.codingchallenge.xmlfilter.controller;

import com.codingchallenge.xmlfilter.controller.response.RequestConfirmationListResponse;
import com.codingchallenge.xmlfilter.controller.response.RequestConfirmationResponse;
import com.codingchallenge.xmlfilter.domain.RequestConfirmation;
import com.codingchallenge.xmlfilter.service.RequestConfirmationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
public class RequestConfirmationControllerTest {

    public static final String TEST_XML = "test-xml";
    public static final String TEST_FILTERID = "test-filterid";
    public static final String TEST_BUYER_1 = "test-buyer1";
    public static final String TEST_SELLER_1 = "test-seller1";
    public static final String TEST_BUYER_2 = "test-buyer2";
    public static final String TEST_SELLER_2 = "test-seller2";

    private RequestConfirmationController requestConfirmationController;

    @Mock
    private RequestConfirmationService requestConfirmationService;

    @Test
    public void testSave() {

        requestConfirmationController = new RequestConfirmationController(requestConfirmationService);

        ResponseEntity responseEntity = requestConfirmationController.saveEvent(TEST_XML);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        Mockito.verify(requestConfirmationService).save(TEST_XML);
    }

    @Test
    public void testGet() {

        requestConfirmationController = new RequestConfirmationController(requestConfirmationService);

        List<RequestConfirmation> requestConfirmations = new ArrayList<>();

        requestConfirmations.add(RequestConfirmation.builder().
                                        buyerParty(TEST_BUYER_1).
                                        sellerParty(TEST_SELLER_1).
                                        premiumAmount(BigDecimal.valueOf(100)).
                                        premiumCurrency("AUD").
                                        build());

        requestConfirmations.add(RequestConfirmation.builder().
                buyerParty(TEST_BUYER_2).
                sellerParty(TEST_SELLER_2).
                premiumAmount(BigDecimal.valueOf(200)).
                premiumCurrency("USD").
                build());

        Mockito.when(requestConfirmationService.getRequestConfirmations(TEST_FILTERID)).thenReturn(requestConfirmations);

        ResponseEntity<RequestConfirmationListResponse> responseEntity = requestConfirmationController.getEvents(TEST_FILTERID);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        RequestConfirmationResponse requestConfirmationResponse = responseEntity.getBody().getRequestConfirmationResponses().get(0);

        Assertions.assertEquals(TEST_BUYER_1, requestConfirmationResponse.getBuyerParty());
        Assertions.assertEquals(TEST_SELLER_1, requestConfirmationResponse.getSellerParty());
        Assertions.assertEquals(BigDecimal.valueOf(100), requestConfirmationResponse.getPremiumAmount());
        Assertions.assertEquals("AUD", requestConfirmationResponse.getPremiumCurrency());

        requestConfirmationResponse = responseEntity.getBody().getRequestConfirmationResponses().get(1);

        Assertions.assertEquals(TEST_BUYER_2, requestConfirmationResponse.getBuyerParty());
        Assertions.assertEquals(TEST_SELLER_2, requestConfirmationResponse.getSellerParty());
        Assertions.assertEquals(BigDecimal.valueOf(200), requestConfirmationResponse.getPremiumAmount());
        Assertions.assertEquals("USD", requestConfirmationResponse.getPremiumCurrency());

    }

}
